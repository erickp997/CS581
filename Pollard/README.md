# Pollard Assignment

> ### Pollard problem. Read the BBC News article: UK’s Vicky Pollard’s left behind. The article gives the following statistic about teen language: “The top 20 words used, including yeah, no, but and like, account for around a third of all words.” The background question is: How many word types account for a third of all word tokens in English in general?

[Full Assignment Source](https://gawron.sdsu.edu/compling/course_core/comp_ling_assignments/pollard_assignment.html)
