# Midterm Tagging Problem

> ### You may need to compute some more counts to answer the next questions, but the model code above together with what you’ve learned on the Pollard and smoothing assignments should be enough to help you answer them. IN any case, you must hand in not just your numbers, but the code used to compute them.

[Full assignment source](https://gawron.sdsu.edu/compling/course_core/assignments/midterm_2019/hmm_tagging_problem_2019.pdf)
