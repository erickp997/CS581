# Erick Pulido
# Dr. Gawron
# CS581
# 11 April 2019

## HMM Problems ##

import nltk
from nltk.corpus import brown

brown_tagged_words = brown.tagged_words()
tags = [t for (w,t) in brown_tagged_words]
words = [w for (w,t) in brown_tagged_words]

tag_bigrams = list(nltk.bigrams(tags))
tag_bigram_cts = nltk.FreqDist(tag_bigrams)
tag_unigram_cts = nltk.FreqDist(tags)

total_bigrams = len(tag_bigrams)
fd_bigrams = len(tag_bigram_cts)
fd_words = nltk.FreqDist(words)

## Problem 1.1.1 ##
AT_JJ_count = tag_bigram_cts['AT', 'JJ']
prob_AT_JJ = float(AT_JJ_count) / total_bigrams
print(prob_AT_JJ) # 0.016782768726247448
V = len(tag_unigram_cts)
V_squared = V * V
smoothed_prob_AT_JJ = (AT_JJ_count + 1) / (total_bigrams + V_squared)
print (smoothed_prob_AT_JJ) # 0.0140819017684568

## Problem 1.1.2 ##
prob_JJ_given_AT = float(tag_bigram_cts['AT', 'JJ']) / tag_unigram_cts['AT']
print (prob_JJ_given_AT) # 0.1989403730131994
prob_JJ_given_AT_smoothed = float(tag_bigram_cts['AT', 'JJ'] + 1) / (tag_unigram_cts['AT'] + V)
print (prob_JJ_given_AT_smoothed) # 0.19799656612246141

## Problem 1.1.3 ##
btw_fd = nltk.FreqDist(brown_tagged_words)
count_AT = tag_unigram_cts['AT']
count_the = fd_words['the']
prob_AT_given_the = float(btw_fd['the','AT']) / count_the
print(prob_AT_given_the) # 0.9932230956898889
prob_the_given_AT = float(btw_fd['the', 'AT']) / count_AT
print(prob_the_given_AT) # 0.6358578589001521

'''
The probability of the part of speech tag AT given the word the is higher than
the probability of the word the given the part of speech tag AT. This is due to
the fact that the word 'the' is one of the most common articles ('AT') where
as the word 'the' can also be an adverb thus it is not always an article. To
reiterate, one of the most common articles is the word 'the', but not all
uses of the word 'the' is articles (i.e adverbs).
'''
