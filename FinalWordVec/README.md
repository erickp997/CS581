# Final Word Vector Problem

> ### You will use slightly different data than the data you used in the word vector assignment, part one. Download this file conspiracy_truncated.zip which includes new data and code you will need to answer the questions below. The code is very similar to the code in the assignment. If you have not done so already, reread the assignment directions about the data and the code.

[Full assignment source](https://gawron.sdsu.edu/compling/course_core/assignments/final_2019/word2Vec_final_problem.html)
