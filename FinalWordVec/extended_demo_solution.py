import os.path
import read_word_vectors
import math
import numpy as np
from scipy.stats.stats import spearmanr

#####################################################
#  Basic numerical PPMI function (optional smoothing)
#####################################################
def ppmi (wv_m, wi, ci, N, wvec=None,cvec=None, k=0,jt_min=1):
    """
    Note the min count value is  set as a default for this function.
    """
    global joint_ct, w_ct, c_ct
    val = wv_m[wi,ci]
    if  val > jt_min:
        joint_ct = (val + k)
        if wvec is None:
            wvec = wv_m[wi,:].toarray()
        if cvec is None:
            cvec = wv_m[:,ci].toarray()
        w_ct = (wvec + k).sum()
        c_ct = (cvec + k).sum()
        N = N + k**2
        #return max(math.log((N*joint_ct)/(w_ct*c_ct) ,2), 0)
        print(type(w_ct),type(c_ct), type(N), type(joint_ct))
        return math.log((N*joint_ct)/(w_ct*c_ct),2)
        # float coercion inserted here for mysterious reasons.
        #return math.log((float(N)*float(joint_ct))/(float(w_ct)*float(c_ct)),2)
    else:
        return 0.0

def look_for_negative_pmi (wvi,N):
    maxi = len(wvi.encoder)
    wv_m = wvi.word_vecs_mx
    for wi in range(maxi):
        wvec = wv_m[wi,:].toarray()
        for ci in range(wi+1, maxi):
            cvec = wv_m[:,ci].toarray()
            if ppmi(wv_m, wi, ci, N, wvec=wvec, cvec=cvec) < 0:
                return wi, ci
            
            
####################################################################
#
#  Nicer similarity system
#
####################################################################

def get_word_vec (wvi,w):
    try:
        # get word index, lookup word vec, render sparse array as dense, flattem to 1D array
        return wvi.word_vecs_mx[wvi.wi[w]].toarray().flatten()
    except KeyError:
        # Also using this as a Boolean Test for whether a word occurs in the data
        return None

def do_sim_score (wvi, w1, w2, convert_to_ppmi=True, normalize=True, col_sums=None, N=None, k=0):
    global wvec1,wvec2
    wvec1,wvec2 = get_word_vec(wvi,w1),get_word_vec(wvi,w2)
    if col_sums is None:
        col_sums = np.array(wvi.word_vecs_mx.sum(axis=0).flat)
    if N is None:
        N = col_sums.sum()
    if convert_to_ppmi:
        wvec1 = ppmi_vector (wvec1, col_sums = col_sums, N=N, k=k)
        wvec2 = ppmi_vector (wvec2, col_sums = col_sums, N=N, k=k)
    if normalize:
        wvec1 = read_word_vectors.normalize_vec(wvec1)
        wvec2 = read_word_vectors.normalize_vec(wvec2)
    return wvec1.dot(wvec2)

############################
#  Vector-level PMI 
############################

def ppmi_stat0(joint_ct, col_ct, N, w_ct, jt_min):
    if joint_ct > jt_min:
        prod1 = w_ct * col_ct 
        if prod1 > 0:
            return max(math.log((N * joint_ct)/prod1, 2), 0)
        else:
            return 0.0
    else:
        return 0.0

#  The tricky thing about this is that it does NOT work if otypes is not set
#  numpy apparently needs to infer the type returned by ppmi_stat0 and it
#  cannot do so correctly wo being told.
ppmi_stat = np.vectorize(ppmi_stat0,otypes=(float,))

def ppmi_vector (wvec, col_sums=None, wv_m=None, N=None,k=0, jt_min=1):
    """
    Must supply either wv_m (word context matrix) or col_sums (vector
    summing the columns of the word context matrix)

    Note the MIN CT value is set as a default for this function.  This is
    the minimum number of tokens a context word must have in teh context of
    a target word to be included in the target word ppmi calculations.
    """
    global ns, w_ct, newN
    if col_sums is None:
        try:
            col_sums = np.array(wv_m.sum(axis=0).flat)
        except AttributeError as e:
            print('** ppmi_vector Error ** Must supply either col_sums vector (col_sums=) or word context matrix (wv_m =)!')
            raise e
    ##  Optional smoothing calcs [k = 0 as default]
    wvec = wvec + k
    col_sums = col_sums+(k*wvec.shape[0])
    if N is None:
        N = col_sums.sum()
    else:
        N = N+k**2
    newN = N
    ####End Smoothing calcs ######
    w_ct = wvec.sum()
    wvec, col_sums, wlen = wvec.flatten(), col_sums.flatten(), wvec.shape[0]
    assert wlen == col_sums.shape[0],  '**PPMI Error** Col sums and word vector have different sizes'
    res = ppmi_stat(wvec, col_sums, N=N, w_ct=w_ct,jt_min=jt_min)
    return res

###############################################
#  Evaluation
###############################################

def read_wordsim_file (fn):
    scores,pairs = [], []
    with open(fn,'r') as fh:
        for line in fh:
            (w1,w2,sc) = [e.strip() for e in line.strip().split()]
            try:
                scores.append(float(sc))
            except ValueError:
                print(line)
                continue
            pairs.append((w1,w2))
    return scores, pairs
        
if __name__ == '__main__':

   
    ####################################################################
    #
    #  Execution params
    #
    ####################################################################

    #Add k smoothing param
    k = 0
    test_ppmi_vector = False
    
    ####################################################################
    #
    #  TEST behavior of ppmi_vector on a toy example
    #
    ####################################################################

    if test_ppmi_vector:
        wind, veclen = 1, 6
        M = np.arange(24).reshape((4,veclen))
        N = M.sum()
        col_sums = M.sum(axis=0)
        wvec = M[wind,:]
        pv = ppmi_vector(wvec, col_sums=col_sums,k=k)
        pv3 = ppmi_vector(wvec, col_sums=col_sums, N=N,k=k)
        pv2 = ppmi_vector(wvec, wv_m=M, N=N,k=k)
        ans = np.array([ppmi (M, wind, x, N, wvec=M[wind,:],cvec=M[:,x]) for x in range(veclen)])
        print(ans)
        print(pv)
        print(pv2)
        print(pv3)

    ####################################################################
    #
    #  Set up code
    #
    ####################################################################

    base = 'conspiracy_truncated'
    # "Current directory": You can edit this if you want the data to be elsewhere
    data_dir = os.getcwd()
    base = os.path.join(data_dir, base)
    #save_wvs = True
    save_wvs = False

    # a "Word Vector Interface" object
    print ('Loading word vector matrix (wvi.wv_m)')
    wvi = read_word_vectors.WordVectorInt(base)
    print ('wv_m loaded')
    # the word vector matrix
    wv_m = wvi.word_vecs_mx


    ####################################################################
    #
    #  Looking up word vectors
    #
    ####################################################################

    # Sum of all the counts in the word vector matrix
    N = wv_m.sum()
    # Get the word index for the Proper Noun (NN) "Trump" [shd be 343]
    Ti = wvi.wi[('Trump','NNP')]
    # Get the word index for the Proper Noun (NNP) "President" [shd be 4224]
    Pi = wvi.wi[('President','NNP')]
    pi = wvi.wi[('president','NN')]
    fi = wvi.wi[('fake','JJ')]
    ni = wvi.wi[('news','NN')]
    pri = wvi.wi[('propaganda','NN')]
    Oi = wvi.wi[('Obama','NNP')]
    ci = wvi.wi[('chicken','NN')]
    bi = wvi.wi[('bus','NN')]
    li = wvi.wi[('legislation','NN')]


    # Look up word vectors
    wv_Trump = wv_m[Ti,:].toarray()
    wv_Obama = wv_m[Oi,:].toarray()
    wv_news =  wv_m[ni,:].toarray()
    wv_chicken =  wv_m[ci,:].toarray()
    wv_bus =  wv_m[bi,:].toarray()
    wv_propaganda =  wv_m[pri,:].toarray()
    wv_legislation =  wv_m[li,:].toarray()

    # Look up context vectors 
    cv_President = wv_m[:,Pi].toarray()
    cv_president = wv_m[:,pi].toarray()
    cv_fake = wv_m[:,fi].toarray()


    print ('vector lookups done')
    ####################################################################
    #
    #  Looking up counts
    #
    ####################################################################


    # Count of the number of times "President_NNP" has occurred as
    # a context word for "Trump_NNP"
    President_Trump_ct = wv_m[Ti,Pi]
    # Count of the number of times "president_NN" has occurred as
    # a context word for "Trump_NNP"
    president_Trump_ct = wv_m[Ti,pi]

    # "Trump" word ct: sum of counts in "Trump_NNP" word vector
    Trump_wc = wv_Trump.sum()
    # "President" context ct: sum of counts in "President_NNP" context vector
    President_cc = cv_President.sum()
    president_cc = cv_president.sum()

    print ('counts done')
    ####################################################################
    #
    #  PPMI calculations
    #
    ####################################################################

    president_Trump_ppmi = ppmi(wv_m, Ti, pi, N, wvec=wv_Trump, cvec=cv_president,k=k)
    print('PPMI score for context word {0} target word {1}:  {2:.4f}'.format('president_NN','Trump_NNP', president_Trump_ppmi))


    fake_news_ppmi = ppmi(wv_m, ni, fi, N, wvec=wv_news, cvec=cv_president,k=k)
    print('PPMI score for context word {0} target word {1}:  {2:.4f}'.format('fake_JJ','news_NN', fake_news_ppmi))

    ####################################################################
    #
    #  Similarity of two vectors
    #
    ####################################################################

    print ('computing similarity of propaganda_NN and news_NN')
    wv_propaganda_norm = read_word_vectors.normalize_vec(wv_propaganda)
    wv_news_norm = read_word_vectors.normalize_vec(wv_news)
    print('Similarity of propaganda and news: {0:.4f}'.format(wv_news_norm.dot(wv_propaganda_norm)))

    wv_Trump_norm = read_word_vectors.normalize_vec(wv_Trump)
    wv_Obama_norm = read_word_vectors.normalize_vec(wv_Obama)
    print('Similarity of Obama and Trump: {0:.4f}'.format(wv_Trump_norm.dot(wv_Obama_norm)))

    wv_chicken_norm = read_word_vectors.normalize_vec(wv_chicken)
    wv_bus_norm = read_word_vectors.normalize_vec(wv_bus)
    print('Similarity of chicken and bus: {0:.4f}'.format(wv_chicken_norm.dot(wv_bus_norm)))

    print('Similarity of chicken and Trump: {0:.4f}'.format(wv_chicken_norm.dot(wv_Trump_norm)))
    print('Similarity of chicken and Obama: {0:.4f}'.format(wv_chicken_norm.dot(wv_Obama_norm)))
    print('Similarity of chicken and propaganda: {0:.4f}'.format(wv_chicken_norm.dot(wv_propaganda_norm)))
    print('Similarity of chicken and news: {0:.4f}'.format(wv_chicken_norm.dot(wv_news_norm)))

    wv_legislation_norm = read_word_vectors.normalize_vec(wv_legislation)
    print('Similarity of chicken and legislation: {0:.4f}'.format(wv_chicken_norm.dot(wv_legislation_norm)))
    print('Similarity of news and legislation: {0:.4f}'.format(wv_news_norm.dot(wv_legislation_norm)))

    L = [('Trump','NNP'), ('President','NNP'), ('president','NN'),
         ('fake','JJ'), ('news','NN'), ('propaganda','NN'), ('Obama','NNP'),
         ('chicken','NN'), ('destruction','NN'),('man','NN'),('boy','NN'), ('girl','NN')]

    col_sums = wv_m.sum(axis=1)
    col_sums = np.array(col_sums.flat)
    sim_scores = [(do_sim_score(wvi, w1, w2,col_sums=col_sums), w1, w2) for (i,w1) in enumerate(L) for w2 in L[i+1:]]
    sim_scores2 = [(do_sim_score(wvi, w1, w2, col_sums=col_sums, convert_to_ppmi=False), w1, w2) for (i,w1) in enumerate(L) for w2 in L[i+1:]]

    for (sc, w1, w2) in sorted(sim_scores, reverse = True):
        wl = ['{0}_{1}'.format(w[0],w[1]) for w in [w1, w2]]
        print('{0:<25} {1:<25} {2:.3f}'.format(wl[0], wl[1], sc))

    print()
    print()

    for (sc, w1, w2) in sorted(sim_scores2, reverse = True):
        wl = ['{0}_{1}'.format(w[0],w[1]) for w in [w1, w2]]
        print('{0:<25} {1:<25} {2:.3f}'.format(wl[0], wl[1], sc))



    eval_data_set = 'MillerCharles'
    eval_file = os.path.join(data_dir,'{0}WordSet.txt'.format(eval_data_set))
    print('Loading {0} word set'.format(eval_data_set))
    human_scores, pairs = read_wordsim_file(eval_file)
    print('{0:d} word pairs in {1} word set loaded'.format(len(pairs), eval_data_set))

    # They are all nouns, add 'NN' pos.  Check for whether each word in the test occurred in the data
    filtered_scores_pairs = [(human_scores[i], ((w1,'NN'),(w2,'NN'))) for (i, (w1,w2)) in enumerate(pairs)
                               if get_word_vec (wvi,(w1,'NN')) is not None and get_word_vec (wvi,(w2,'NN')) is not None]
    filtered_human_scores, filtered_pairs = zip(*filtered_scores_pairs)
    print('{0:d} word pairs after filtering'.format(len(filtered_pairs)))

    print('Evaluating word systems with and without PPMI vector values')
    sys_scores = [do_sim_score(wvi, w1, w2,col_sums=col_sums,k=k) for (w1,w2) in filtered_pairs]
    sys_scores2 = [do_sim_score(wvi, w1, w2,col_sums=col_sums, convert_to_ppmi=False, k=k) for (w1,w2) in filtered_pairs]
    corr,sig = spearmanr(filtered_human_scores, sys_scores)
    corr2,sig2 = spearmanr(filtered_human_scores, sys_scores2)

    print('PMI sys: {0:.3f} No PMI sys:{1:.3f}'.format(corr, corr2))
    
'''
Beginning Code for Final Word Vector Problem Part 1
Erick Pulido
Dr. Gawron
CS581
16 May 2019
'''

# 1. PPMI score for vocab word cabal_NN, context word global_JJ.
cabal_index = wvi.wi[('cabal','NN')]
global_index = wvi.wi[('global', 'JJ')]
wv_cabal = wv_m[cabal_index,:].toarray()
wv_global = wv_m[global_index,:].toarray()
cv_global = wv_m[:,global_index].toarray()
ppmi_global_cabal = ppmi(wv_m, cabal_index, global_index, N, wvec=wv_cabal, cvec=cv_global, k=k)
print(ppmi_global_cabal) # 4.759867655813557

# 2. PPMI score for context word cabal_NN and target word international_JJ.
international_index = wvi.wi[('international','JJ')]
wv_international = wv_m[international_index,:].toarray()
cv_cabal = wv_m[:,cabal_index].toarray()
ppmi_international_cabal = ppmi(wv_m, international_index, cabal_index, N, wvec=wv_international, cvec=cv_cabal, k=k)
print(ppmi_international_cabal) # 0

# 3. PPMI score for target word agenda_NN and context word liberal_JJ.
agenda_index = wvi.wi[('agenda','NN')]
liberal_index = wvi.wi[('liberal','JJ')]
wv_agenda = wv_m[agenda_index,:].toarray()
wv_liberal = wv_m[liberal_index,:].toarray()
cv_liberal = wv_m[:,liberal_index].toarray()
ppmi_liberal_agenda = ppmi(wv_m, agenda_index, liberal_index, N, wvec=wv_agenda, cvec=cv_liberal, k=k)
print(ppmi_liberal_agenda) # 4.1053969171310625

# 4. PPMI score for target word liberal_JJ and context word agenda_NN.
cv_agenda = wv_m[:,agenda_index].toarray()
ppmi_agenda_liberal = ppmi(wv_m, liberal_index, agenda_index, N, wvec=wv_liberal, cvec=cv_agenda, k=k)
print(ppmi_agenda_liberal) # 4.186319934273267

# 5. The answers to (3) and (4) are very close. Is this a surprising fact about the data, an expected fact about the data, or is it not a fact about the data at all?

'''
This fact should not be surprising considering the fact that the questions
swapped the role of the words, but kept the same two words. The positive 
pointwise mutual information indicates in its name that mutual information is
looked at and when the roles of the words switch there is still plenty of
mutual information. 
'''

# 6. Cosine similarity of vocab word cabal_NN and vocab word conspiracy_NN, using word vectors with counts.
conspiracy_index = wvi.wi[('conspiracy','NN')]
wv_conspiracy = wv_m[conspiracy_index,:].toarray()
wv_cabal_norm = read_word_vectors.normalize_vec(wv_cabal)
wv_conspiracy_norm = read_word_vectors.normalize_vec(wv_conspiracy)
cabal_conspiracy_similar_counts = wv_cabal_norm.dot(wv_conspiracy_norm)
print(cabal_conspiracy_similar_counts) # 0.7994512852414405

# 7. Cosine similarity of vocab word cabal_NN and vocab word conspiracy_NN, using word vectors with PPMI values. (Note: You may find it useful to borrow some code from extended_demo_solution.py)
ppmi_cabal_conspiracy_similarity = do_sim_score(wvi, ('cabal','NN'),('conspiracy','JJ'))
print(ppmi_cabal_conspiracy_similarity) # 0.017741384250418962

# 8. Cosine similarity of vocab word global_JJ and vocab word international_JJ, using word vectors with counts.
wv_global_norm = read_word_vectors.normalize_vec(wv_global)
wv_international_norm = read_word_vectors.normalize_vec(wv_international)
global_international_similar_counts = wv_global_norm.dot(wv_international_norm)
print(global_international_similar_counts) # 0.8767149072967063

# 9. Cosine similarity of vocab word politician_NN and vocab word criminal_NN, using word vectors with PPMI values.
ppmi_politician_criminal_similarity = do_sim_score(wvi, ('politician','NN'),('criminal','NN'))
print(ppmi_politician_criminal_similarity) # 0.0574980134405934
