import os.path
import read_word_vectors
import math
import numpy as np

#####################################################
#  Basic numerical PPMI function (optional smoothing)
#####################################################

def ppmi (wv_m, wi, ci, N, wvec=None,cvec=None, k=0,jt_min=1):
    """
    Note a min count value is  set as a default for this function.
    """
    global joint_ct, w_ct, c_ct
    val = wv_m[wi,ci]
    if  val > jt_min:
        joint_ct = (val + k)
        if wvec is None:
            wvec = wv_m[wi,:].toarray()
        if cvec is None:
            cvec = wv_m[:,ci].toarray()
        w_ct = (wvec + k).sum()
        c_ct = (cvec + k).sum()
        N = N + k**2
        return max(math.log((N*joint_ct)/(w_ct*c_ct) ,2), 0)
    else:
        return 0.0
        
if __name__ == '__main__':

   
    ####################################################################
    #
    #  Execution params
    #
    ####################################################################

    #Add k smoothing param
    k = 0

    ####################################################################
    #
    #  Set up code
    #
    ####################################################################

    #base = 'conspiracy_mid'
    base = 'conspiracy_truncated'
    # "Current directory": You can edit this if you want the data to be elsewhere
    data_dir = os.getcwd()
    base = os.path.join(data_dir, base)
    save_wvs = False

    # a "Word Vector Interface" object
    print ('Loading word vector matrix (wvi.wv_m)')
    wvi = read_word_vectors.WordVectorInt(base)
    print ('wv_m loaded')
    # the word vector matrix
    wv_m = wvi.word_vecs_mx


    ####################################################################
    #
    #  Looking up word vectors
    #
    ####################################################################

    # Sum of all the counts in the word vector matrix
    N = wv_m.sum()
    # Get the word index for the Proper Noun (NN) "Trump" [shd be 343]
    Ti = wvi.wi[('Trump','NNP')]
    # Get the word index for the Proper Noun (NNP) "President" [shd be 4224]
    Pi = wvi.wi[('President','NNP')]
    pi = wvi.wi[('president','NN')]
    fi = wvi.wi[('fake','JJ')]
    ni = wvi.wi[('news','NN')]
    pri = wvi.wi[('propaganda','NN')]
    Oi = wvi.wi[('Obama','NNP')]
    ci = wvi.wi[('chicken','NN')]
    bi = wvi.wi[('bus','NN')]
    li = wvi.wi[('legislation','NN')]


    # Look up word vectors
    wv_Trump = wv_m[Ti,:].toarray()
    wv_Obama = wv_m[Oi,:].toarray()
    wv_news =  wv_m[ni,:].toarray()
    wv_chicken =  wv_m[ci,:].toarray()
    wv_bus =  wv_m[bi,:].toarray()
    wv_propaganda =  wv_m[pri,:].toarray()
    wv_legislation =  wv_m[li,:].toarray()

    # Look up context vectors 
    cv_President = wv_m[:,Pi].toarray()
    cv_president = wv_m[:,pi].toarray()
    cv_fake = wv_m[:,fi].toarray()


    print ('vector lookups done')
    ####################################################################
    #
    #  Looking up counts
    #
    ####################################################################


    # Count of the number of times "President_NNP" has occurred as
    # a context word for "Trump_NNP"
    President_Trump_ct = wv_m[Ti,Pi]
    # Count of the number of times "president_NN" has occurred as
    # a context word for "Trump_NNP"
    president_Trump_ct = wv_m[Ti,pi]

    # "Trump" word ct: sum of counts in "Trump_NNP" word vector
    Trump_wc = wv_Trump.sum()
    # "President" context ct: sum of counts in "President_NNP" context vector
    President_cc = cv_President.sum()
    president_cc = cv_president.sum()

    print ('counts done')
    ####################################################################
    #
    #  PPMI calculations
    #
    ####################################################################

    president_Trump_ppmi = ppmi(wv_m, Ti, pi, N, wvec=wv_Trump, cvec=cv_president,k=k)
    print('PPMI score for context word {0} target word {1}:  {2:.4f}'.format('president_NN','Trump_NNP', president_Trump_ppmi))


    fake_news_ppmi = ppmi(wv_m, ni, fi, N, wvec=wv_news, cvec=cv_president,k=k)
    #print('Testing: {0}'.format(ppmi(wv_m, ni, fi, N, wvec=wv_news, cvec=cv_president)[1] == fake_news_ppmi))
    print('PPMI score for context word {0} target word {1}:  {2:.4f}'.format('fake_JJ','news_NN', fake_news_ppmi))

    ####################################################################
    #
    #  Similarity of two vectors
    #
    ####################################################################

    print ('computing similarity of propaganda_NN and news_NN')
    wv_propaganda_norm = read_word_vectors.normalize_vec(wv_propaganda)
    wv_news_norm = read_word_vectors.normalize_vec(wv_news)
    print('Similarity of propaganda and news: {0:.4f}'.format(wv_news_norm.dot(wv_propaganda_norm)))

    wv_Trump_norm = read_word_vectors.normalize_vec(wv_Trump)
    wv_Obama_norm = read_word_vectors.normalize_vec(wv_Obama)
    print('Similarity of Obama and Trump: {0:.4f}'.format(wv_Trump_norm.dot(wv_Obama_norm)))

    wv_chicken_norm = read_word_vectors.normalize_vec(wv_chicken)
    wv_bus_norm = read_word_vectors.normalize_vec(wv_bus)
    print('Similarity of chicken and bus: {0:.4f}'.format(wv_chicken_norm.dot(wv_bus_norm)))

    print('Similarity of chicken and Trump: {0:.4f}'.format(wv_chicken_norm.dot(wv_Trump_norm)))
    print('Similarity of chicken and Obama: {0:.4f}'.format(wv_chicken_norm.dot(wv_Obama_norm)))
    print('Similarity of chicken and propaganda: {0:.4f}'.format(wv_chicken_norm.dot(wv_propaganda_norm)))
    print('Similarity of chicken and news: {0:.4f}'.format(wv_chicken_norm.dot(wv_news_norm)))
