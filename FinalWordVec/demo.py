####################################################################
#
#  Set up code
#
####################################################################

import os.path
import read_word_vectors
import math
import numpy as np

base = 'conspiracy_mid'
# "Current directory": You can edit this if you want the data to be elsewhere
data_dir = os.getcwd()
base = os.path.join(data_dir, base)
# a "Word Vector Interface" object
wvi = read_word_vectors.WordVectorInt(base)
# the word vector matrix
wv_m = wvi.word_vecs_mx


####################################################################
#
#  Looking up word vectors
#
####################################################################

# Sum of all the counts in the word vector matrix
N = wv_m.sum()
# Get the word index for the Proper Noun (NN) "Trump" [shd be 343]
Ti = wvi.wi[('Trump','NNP')]
# Get the word index for the Proper Noun (NNP) "President" [shd be 4224]
Pi = wvi.wi[('President','NNP')]
pi = wvi.wi[('president','NN')]
ci = wvi.wi[('chicken','NN')]
bi = wvi.wi[('bus','NN')]

# Using Ti, look up word vector for "Trump" (a ROW in wv_m)
wv_Trump = wv_m[Ti,:].toarray()
# Context vector for "President" (a COLUMN in wv_m)
cv_President = wv_m[:,Pi].toarray()
cv_president = wv_m[:,pi].toarray()



####################################################################
#
#  Looking up counts
#
####################################################################


# Count of the number of times "President_NNP" has occurred as
# a context word for "Trump_NNP"
President_Trump_ct = wv_m[Ti,Pi]
# Count of the number of times "president_NN" has occurred as
# a context word for "Trump_NNP"
president_Trump_ct = wv_m[Ti,pi]

# "Trump" word ct: sum of counts in "Trump_NNP" word vector
Trump_wc = wv_Trump.sum()
# "President" context ct: sum of counts in "President_NNP" context vector
President_cc = cv_President.sum()
president_cc = cv_president.sum()

####################################################################
#
#  Doing the math
#
####################################################################

# Take the log (base 2) of 276
math.log(276,2)

####################################################################
#
#  Similarity of two vectors
#
####################################################################

# Cook up a silly vector with the numbers 0...23
vec1 = np.arange(24)
# Divide this vector by its length to get a vector of length 1
vec1_norm = read_word_vectors.normalize_vec(vec1)

# Cook up another silly vector with the numbers 23...0
vec2 = np.arange(23,-1,-1)
vec2_norm = read_word_vectors.normalize_vec(vec2)

# Check out the normed vec length to see if it's one.
# We compute the len squared, to save taking a square root
len_vec1_norm_sqd = sum(vec1_norm**2)

# We compute the cosine similarity by taking
# the dot product of the normalized vectors

vec1_vec2_sim = vec1_norm.dot(vec2_norm)






    


