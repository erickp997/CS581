# Smoothing Assignment

> ### In the Pollard assignment you computed a unigram frequency distribution for the Brown corpus. You will need that for this assignment.

[Full assignment source](https://gawron.sdsu.edu/compling/course_core/comp_ling_assignments/smoothing_assignment.html)
