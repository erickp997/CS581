import nltk
from nltk.corpus import brown
from nltk import bigrams
words = brown.words()
fdist = nltk.FreqDist([w.lower() for w in words])
bigram_sequence = list(nltk.bigrams(words))
bigram_N = len(bigram_sequence)
mle_bigram_distribution = nltk.FreqDist([(x.lower(),y.lower()) for (x, y) in bigram_sequence])
p_sewer_brother = mle_bigram_distribution[('sewer', 'brother')]
count_in_the = mle_bigram_distribution[('in','the')]
p_the_given_in = float(mle_bigram_distribution[('in', 'the')])/fdist['in']
p_in_the = mle_bigram_distribution[('in', 'the')]/float(bigram_N)
count_said_the = mle_bigram_distribution[('said','the')]
p_the_given_said = float(mle_bigram_distribution[('said','the')])/float(fdist['said'])
p_said_the = float(mle_bigram_distribution[('said','the')])/float(bigram_N)
V = len(fdist)
bigram_size = V**2
smoothed_bigram_N = bigram_N + bigram_size
from collections import defaultdict
laplace_uni = defaultdict(lambda: 0)
for x in fdist:
    laplace_uni[x] = fdist[x] + V

laplace_bigram_distribution = defaultdict(lambda: 1)
for big in mle_bigram_distribution:
    laplace_bigram_distribution[big] = mle_bigram_distribution[big] + 1

smoothed_count_in_the = laplace_bigram_distribution[('in', 'the')]
smoothed_p_the_given_in = float(laplace_bigram_distribution[('in','the')])/laplace_uni['in']
smoothed_p_in_the = laplace_bigram_distribution[('in', 'the')]/float(smoothed_bigram_N)
smoothed_count_said_the = laplace_bigram_distribution[('said','the')]
smoothed_p_the_given_said = float(laplace_bigram_distribution[('said','the')])/laplace_uni['said']
smoothed_p_said_the = float(laplace_bigram_distribution[('said','the')])/float(smoothed_bigram_N)
smoothed_p_sewer_brother = float(laplace_bigram_distribution[('sewer','brother')])/float(smoothed_bigram_N)
print ("P(the | in)")
print(p_the_given_in, smoothed_p_the_given_in)
print ("P(in the)")
print(p_in_the, smoothed_p_in_the)
print ("P(the | said)")
print(p_the_given_said, smoothed_p_the_given_said)
print("P(said the)")
print(p_said_the, smoothed_p_said_the)
print("P(sewer brother)")
print(p_sewer_brother, smoothed_p_sewer_brother)
