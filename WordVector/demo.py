####################################################################
#
#  Set up code
#
####################################################################

import os.path
import read_word_vectors
import math
import numpy as np

base = 'conspiracy_mid'
# "Current directory": You can edit this if you want the data to be elsewhere
data_dir = os.getcwd()
base = os.path.join(data_dir, base)
# a "Word Vector Interface" object
wvi = read_word_vectors.WordVectorInt(base)
# the word vector matrix
wv_m = wvi.word_vecs_mx


####################################################################
#
#  Looking up word vectors
#
####################################################################

# Sum of all the counts in the word vector matrix
N = wv_m.sum()
# Get the word index for the Proper Noun (NN) "Trump" [shd be 343]
Ti = wvi.wi[('Trump','NNP')]
# Get the word index for the Proper Noun (NNP) "President" [shd be 4224]
Pi = wvi.wi[('President','NNP')]
pi = wvi.wi[('president','NN')]
ci = wvi.wi[('chicken','NN')]
bi = wvi.wi[('bus','NN')]

# Using Ti, look up word vector for "Trump" (a ROW in wv_m)
wv_Trump = wv_m[Ti,:].toarray()
# Context vector for "President" (a COLUMN in wv_m)
cv_President = wv_m[:,Pi].toarray()
cv_president = wv_m[:,pi].toarray()



####################################################################
#
#  Looking up counts
#
####################################################################


# Count of the number of times "President_NNP" has occurred as
# a context word for "Trump_NNP"
President_Trump_ct = wv_m[Ti,Pi]
# Count of the number of times "president_NN" has occurred as
# a context word for "Trump_NNP"
president_Trump_ct = wv_m[Ti,pi]

# "Trump" word ct: sum of counts in "Trump_NNP" word vector
Trump_wc = wv_Trump.sum()
# "President" context ct: sum of counts in "President_NNP" context vector
President_cc = cv_President.sum()
president_cc = cv_president.sum()

####################################################################
#
#  Doing the math
#
####################################################################

# Take the log (base 2) of 276
math.log(276,2)

####################################################################
#
#  Similarity of two vectors
#
####################################################################

# Cook up a silly vector with the numbers 0...23
vec1 = np.arange(24)
# Divide this vector by its length to get a vector of length 1
vec1_norm = read_word_vectors.normalize_vec(vec1)

# Cook up another silly vector with the numbers 23...0
vec2 = np.arange(23,-1,-1)
vec2_norm = read_word_vectors.normalize_vec(vec2)

# Check out the normed vec length to see if it's one.
# We compute the len squared, to save taking a square root
len_vec1_norm_sqd = sum(vec1_norm**2)

# We compute the cosine similarity by taking
# the dot product of the normalized vectors

vec1_vec2_sim = vec1_norm.dot(vec2_norm)

'''
Begin Assignment Part 1, from here on out:
Erick Pulido
Dr. Gawron
CS581
7 May 2019
'''

# 1. Calculate PPMI score for vocab word Trump, context word President.
prob_President_Trump = president_Trump_ct / N
prob_president = president_cc / N
prob_Trump = Trump_wc / N
PPMI_President_Trump = math.log(prob_President_Trump / (prob_president * prob_Trump), 2)
print(PPMI_President_Trump) # 2.872799803591284

# 2. Calculate PPMI score for target word news, context word fake.
news_i = wvi.wi[('news', 'NN')]
fake_i = wvi.wi[('fake', 'JJ')]
wv_news = wv_m[news_i,:].toarray()
cv_fake = wv_m[:,fake_i].toarray()
fake_news_ct = wv_m[news_i,fake_i]
news_wc = wv_news.sum()
fake_cc = cv_fake.sum()
prob_fake_news = fake_news_ct / N
prob_fake = fake_cc / N
prob_news = news_wc / N
PPMI_fake_news = math.log(prob_fake_news / (prob_fake * prob_news), 2)
print(PPMI_fake_news) # 6.262367488936338

# 3. Cosine similarity of vocab word propoganda and vocab word news.
propoganda_i = wvi.wi[('propoganda', 'NN')]
wv_propoganda = wv_m[propoganda_i,:].toarray()
wv_propoganda_norm = read_word_vectors.normalize_vec(wv_propoganda)
wv_news_norm = read_word_vectors.normalize_vec(wv_news)
len_wv_prop_norm_sqd = sum(x**2 for x in wv_propoganda_norm)
len_wv_news_norm_sqd = sum(x**2 for x in wv_news_norm)
news_propoganda_cosine_sim = wv_propoganda_norm.dot(wv_news_norm)
print(news_propoganda_cosine_sim) # 0.6710638544036989

# 4. Cosine similarity of vocab word Trump and vocab word Obama.
Obama_i = wvi.wi[('Obama', 'NNP')]
wv_Obama = wv_m[Obama_i,:].toarray()
wv_Obama_norm = read_word_vectors.normalize_vec(wv_Obama)
wv_Trump_norm = read_word_vectors.normalize_vec(wv_Trump)
len_wv_Obama_norm_sqd = sum(x**2 for x in wv_Obama_norm)
len_wv_Trump_norm_sqd = sum(x**2 for x in wv_Trump_norm)
Trump_Obama_cosine_sim = wv_Trump_norm.dot(wv_Obama_norm)
print(Trump_Obama_cosine_sim) # 0.8917485907332634
    
