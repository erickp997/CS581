# Word Vector Assignment

> ### This assignment came in two parts. In the first part you computed some sparse distributional vectors using the classic approach, in which each position in a word vector represents occurrences of a context word with some fixed window size of the target word (we used 5 as the window size in this assignment).

[Full assingment source](https://gawron.sdsu.edu/compling/course_core/comp_ling_assignments/word2vec.html)
