"""
wvi = read_word_vectors.WordVectorInt(base)
# the word vector matrix
wv_m = wvi.word_vecs_mx


####################################################################
#
#  Looking up word vectors
#
####################################################################

# Sum of all the counts in the word vector matrix
N = wv_m.sum()
# Get the word index for the Proper Noun (NN) "Trump" [shd be 343]
Ti = wvi.wi[('Trump','NNP')]
# Get the word index for the Proper Noun (NNP) "President" [shd be 4224]
Pi = wvi.wi[('President','NNP')]
pi = wvi.wi[('president','NN')]

# Using Ti, look up word vector for "Trump" (a ROW in wv_m)
wv_Trump = wv_m[Ti,:]
# Context vector for "President" (a COLUMN in wv_m)
cv_President = wv_m[:,Pi]
cv_president = wv_m[:,pi]


####################################################################
#
#  Looking up counts
#
####################################################################


# Count of the number of times "President_NNP" has occurred as
# a context word for "Trump_NNP"
President_Trump_ct = wv_m[Ti,Pi]
# Count of the number of times "president_NN" has occurred as
# a context word for "Trump_NNP"
president_Trump_ct = wv_m[Ti,pi]

# "Trump" word ct: sum of counts in "Trump_NNP" word vector
Trump_wc = wv_Trump.sum()
# "President" context ct: sum of counts in "President_NNP" context vector
President_cc = cv_President.sum()
president_cc = cv_president.sum()

####################################################################
#
#  Similarity of two vectors
#
####################################################################

# Cook up a silly vector with the numbers 0...23
vec1 = np.arange(24)
# Divide this vector by its length to get a vector of length 1
vec1_norm = normalize_vec(vec1)

# Cook up another silly vector with the numbers 23...0
vec2 = np.arange(23,-1,-1)
vec2_norm = normalize_vec(vec2)

# Check out the vec length.
# We compute the len squared, to save taking a square root
len_vec1_norm_sqd = sum(x**2 for x in vec1_norm)

# We compute the cosine similarity by taking
# the dot product of the normalized vectors

vec1_vec2_sim = vec1_norm.dot(vec2_norm)

"""

from collections import defaultdict, Counter
import codecs
import numpy as np
from scipy.sparse import csr_matrix, lil_matrix, save_npz
from sklearn.metrics.pairwise import normalize

class WordVectorInt:

    def __init__(self,domain, word_vecs_file=None,
                 word_vecs_file_npy=None,vocab_file=None,
                 tagged=True, load_numpy=False):
        self.base = domain
        self.word_vecs = defaultdict(Counter)
        if vocab_file is not None:
            self.vocab_file = vocab_file
        if word_vecs_file is not None:
            self.word_vecs_file = word_vecs_file
        if vocab_file is None:
            self.vocab_file = self.base + '.vocab'
        if word_vecs_file is None:
            self.word_vecs_file = self.base + '.word_vecs'
        if word_vecs_file_npy is None:
            self.word_vecs_file_npy = self.base + '_word_vecs.npy'
        self.tagged = tagged
        self.load_numpy = load_numpy
        self.vocab = []
        self.encoder = dict()
        self.wi = self.encoder
        self._cur_ind = -1
        self.word_freqs = Counter()
        if tagged:
            self.freq_test_word = ('the', 'DT')
        else:
            self.freq_test_word = 'the'
        self.load_vocab()
        if load_numpy:
             self.load_word_vecs_npy()
        else:
            self.load_word_vecs()
        

    def load_vocab(self):
        print ('Loading vocab')
        with codecs.open(self.vocab_file, 'r', encoding = 'utf8') as fh:
            for (i,line) in enumerate(fh):
                if self.tagged:
                    (form, tag, ct) = [elem.strip() for elem in line.strip().split(u'\t')]
                    #word = u'{0}_{1}'.format(form, tag)
                    word = (form, tag)
                else:
                    (word, ct) = [elem.strip() for elem in line.strip().split(u'\t')]
                ct = int(ct)
                self.add_to_vocab(word)
                self.word_freqs[i] = ct
        self.do_vocab_stats()
        test_ind = self.encoder[self.freq_test_word]
        test_freq = self.word_freqs[test_ind]
        print ('Vocab loaded')
        print ('Test word {0}  Freq: {1}'.format(self.freq_test_word, test_freq))

    def add_to_vocab(self, word):
        word_ind = self.get_word_index (word)
        return word_ind

    def get_word_index (self, word, warn=False):
        if word in self.encoder:
            return self.encoder[word]
        else:
            next_ind = self.get_next_ind()
            self.encoder[word] = next_ind
            self.vocab.append(word)
            if warn:
                print ((len(self.encoder), len(self.vocab)))
                print ('{0}[{1}] is a previously unseen word'.format(word, next_ind))
            return next_ind

    def get_next_ind (self):
        next_ind = self._cur_ind + 1
        self._cur_ind = next_ind
        return next_ind

    def old_load_word_vecs (self):
        print ('Loading word vecs {0}'.format(self.word_vecs_file))
        word_vecs = self.word_vecs
        with open(self.word_vecs_file, 'r') as fh:
            self.line_ctr = 0
            for (index, line) in enumerate(fh):
                if line.strip():
                    self.line_ctr += 1
                    vec_entries = [pair.split(':')  for pair in line.strip().split()
                                   if len(pair.split(':')) == 2]
                    #print('len vec entries: {0:d}'.format(len(vec_entries)))
                    if vec_entries:
                        try:
                            #vec_entries = [(int(word_index), float(pmi_val)) for (word_index,pmi_val) \
                            #               in vec_entries]
                           vec_entries = {int(word_index):round(float(ct)) for (word_index,ct) \
                                           in vec_entries}
                        except (ValueError,TypeError):
                             print ('Badly formatted word vec line {0} {1}'.format(index,line))
                             continue
                    else:
                        print ('Badly formatted word vec line {0} {1}'.format(index,line))
                    #word_vecs[index] = dict(vec_entries)
                    word_vecs[index] = vec_entries
        word_vec_vocab_size =  max(self.word_vecs.keys()) + 1
        assert word_vec_vocab_size == self.vocab_size, '*** Vector Loading Error *** Wrong number of vectors in word vecs file ({0}) relative to current lexicon size ({1})!'.format(word_vec_vocab_size,self.vocab_size)
        if self.use_csr_matrix:
            self.word_vecs_mx = self.convert_pmi_dict_to_csr_matrix()

    def load_word_vecs (self,dt='int32'):
        vocab = self.vocab
        rows = len(vocab)
        cols = rows
        word_vecs = lil_matrix((rows,cols), dtype=dt)
        #word_vecs = np.zeros((rows,cols + 1), dtype=dt)
        self.word_vecs_mx = word_vecs
        print ('Loading word vecs {0}'.format(self.word_vecs_file))
        with open(self.word_vecs_file, 'r') as fh:
            self.line_ctr = 0
            for (index, line) in enumerate(fh):
                self.line_ctr += 1
                if line.strip():
                    vec_entries = [pair.split(':')  for pair in line.strip().split()
                                   if len(pair.split(':')) == 2]
                    #print('len vec entries: {0:d}'.format(len(vec_entries)))
                    if vec_entries:
                        try:
                            #vec_entries = [(int(word_index), int(float(val))) for (word_index,val) \
                            #               in vec_entries]
                            try:
                                vec_entries = [(int(word_index),int(val)) for (word_index,val) \
                                               in vec_entries]
                            except (ValueError,TypeError):
                                vec_entries = [(int(word_index),round(float(ct))) for (word_index,ct) \
                                               in vec_entries]
                            for (c_word_index,ct) in vec_entries:
                                 word_vecs[index,c_word_index] = ct
                        except (ValueError,TypeError):
                             print ('Badly formatted word vec line {0} {1}'.format(index,line))
                             continue
                    else:
                        print ('Badly formatted word vec line {0} {1}'.format(index,line))
                    #word_vecs[index] = dict(vec_entries)
                    #word_vecs[index] = vec_entries
        word_vec_vocab_size =  self.line_ctr #max(self.word_vecs.keys()) + 1
        assert word_vec_vocab_size == self.vocab_size,  '*** Vector Loading Error *** Wrong number of vectors in word vecs file ({0}) relative to current lexicon size ({1})!'.format(word_vec_vocab_size,self.vocab_size)

    def save_word_vecs (self):
        if not hasattr(self, 'vocab_size'):
            w_freqs = self.word_freqs
            if isinstance(w_freqs, dict):
                vocab_size = max(w_freqs.keys()) + 1
            else:
                vocab_size = len(w_freqs)
        else:
            vocab_size = self.vocab_size
        print ('Saving word_vecs file {0}'.format(self.word_vecs_file))
        word_vecs_mx = self.word_vecs_mx
        with open(self.word_vecs_file,'w') as ofh:
            for word_ind in range(vocab_size):
                try:
                    word_vec = word_vecs_mx[word_ind,:]
                except KeyError:
                    continue
                #for (key, val) in word_vec.items():
                #    print('{0:d}:{1:d}'.format(key,val), end=' ', file=ofh)
                rows,cols = word_vecs_mx.shape
                for i in range(rows):
                    for j in range(cols):
                        val = word_vecs_mx[i,j]
                        if val > 0:
                            print('{0:d}:{1:d}'.format(j,val), end=' ', file=ofh)
                    print(file=ofh)


    def old_save_word_vecs (self):
        if not hasattr(self, 'vocab_size'):
            w_freqs = self.word_freqs
            if isinstance(w_freqs, dict):
                vocab_size = max(w_freqs.keys()) + 1
            else:
                vocab_size = len(w_freqs)
        else:
            vocab_size = self.vocab_size
        print ('Saving word_vecs file {0}'.format(self.word_vecs_file))
        with open(self.word_vecs_file,'w') as ofh:
            for word_ind in range(vocab_size):
                try:
                    word_vec = self.word_vecs[word_ind]
                except KeyError:
                    continue
                for (key, val) in word_vec.items():
                    print('{0:d}:{1:d}'.format(key,val), end=' ', file=ofh)
                print(file=ofh)


    def do_vocab_stats(self):
        global freq_dict
        freq_dict = self.word_freqs
        self.vocab_size = len(self.vocab)
        self.word_freqs = np.zeros((self.vocab_size),dtype=int)
        print ('len word_freqs dict: {0} len vocab {1}'.format(len(freq_dict),
                                                              len(self.vocab)))
        for (w_ind, ct) in freq_dict.items():
            self.word_freqs[w_ind] = ct
        self.N = self.word_freqs.sum()

    def convert_pmi_dict_to_csr_matrix(self,topn=0, pos = None,dt='int32'):
        """
        word_vecs is a pmi dict.
        vocab is the vocab sequence which labels the rows of the matrix.

        X = convert_pmi_dict_to_sparse_matrix(word_vecs)
        X is a numpy csr sparse matrix

        """
        vocab = self.vocab
        rows = len(vocab)
        cols = max(col_num for row_num in self.word_vecs.keys()
                   for col_num in self.word_vecs[row_num].keys())
        #lil_m = lil_matrix((rows,cols + 1), dtype=dt)
        lil_m = np.zeros((rows,cols + 1), dtype=dt)
        ctr = 0
        #cols, ctr = 0, 0
        #row_vec, col_vec, data_vec = [ ], [ ], [ ]
        #for row_num in range(topn):
        for (row_num, wd) in enumerate(vocab):
            if topn and ctr > topn:
                break
            if pos is not None and wd[1] != pos:
                continue
            try:
              word_vec = self.word_vecs[row_num]
            except KeyError:
              # freq standards not met for this wd, no word vec constructed.
              continue
            ctr += 1
            for (col_num, val) in word_vec.items():
                #row_vec.append(row_num)
                #col_vec.append(col_num)
                #data_vec.append(val)
                #if col_num > cols:
                #    cols = col_num
                lil_m[row_num, col_num] = val
        #return csr_matrix((data_vec, (row_vec, col_vec)), shape = (rows, cols))
        #return lil_m.asformat('csr')
        return lil_m

    def load_word_vecs_npy (self):
        if not hasattr(self, 'word_vecs_file_npy'):
            self.word_vecs_file_npy = self.base + '_word_vecs.npy'
        print ('Loading npz word vecs {0}'.format(self.word_vecs_file_npy))
        self.word_vecs_mx = np.load(self.word_vecs_file_npy)        

    def save_word_vecs_npy (self):
        if not hasattr(self, 'word_vecs_file_npy'):
            self.word_vecs_file_npy = self.base + '_word_vecs.npy'
        print ('Saving npz word vecs {0}'.format(self.word_vecs_file_npy))
        np.save(self.word_vecs_file_npy,wv_m)


def normalize_vec (vec):
    if vec.ndim < 2:
        m = np.vstack((vec,))
    else:
        m = vec
    sm = normalize(m)
    return sm[0]

if __name__ == '__main__':
    import os.path
    base = 'conspiracy_truncated'
    curdir = os.getcwd()
    base = os.path.join(curdir, base)
    #save_wvs = True
    save_wvs = False

    # a "Word Vector Interface" object
    wvi = WordVectorInt(base)
    # the word vector matrix
    wv_m = wvi.word_vecs_mx


    if save_wvs:
        wvi.save_word_vecs()
    
    # Get the word index for the Proper Noun (NNP) "President"
    Pi = wvi.wi[('President','NNP')]
    pi = wvi.wi[('president','NN')]
    ti = wvi.wi[('Trump','NNP')]
    President_Trump_ct = wv_m[ti,Pi]
    president_Trump_ct = wv_m[ti,pi]

    # Context vector for "President"
    cv_President = wv_m[:,Pi]
    cv_president = wv_m[:,pi]
    # Word vector for "Trump"
    wv_Trump = wv_m[ti,:]




    # "Trump" word ct: sum of counts in Trump word vector
    Trump_wc = wv_Trump.sum()
    # "President" context ct: sum of counts in President context vector
    President_cc = cv_President.sum()
    president_cc = cv_president.sum()
