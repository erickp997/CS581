import nltk
from nltk.corpus import senseval
from sklearn.utils import shuffle

# DATA

hard_data = [(i, i.senses[0]) for i in senseval.instances('hard.pos')]
'''
print(len(hard_data))
print(hard_data[0])
'''
ex0 = hard_data[0]
cls = ex0[1]
#print(cls)

si = ex0[0]
'''
print(si)
print(si.context)
print(si.word)
'''
def find_examples(data, sense, num_examples):
    res = []
    for(inst, s) in data:
        if s == sense:
            res.append(inst)
        if len(res) == num_examples:
            return res
            
def print_examples(ex_list):
    for ex in ex_list:
        for word in ex.context:
            print('{0}_{1}'.format(word[0], word[1]))
        print()
        print()

# TRAINING

hard1_ex = find_examples(hard_data, 'HARD1', 3)
hard2_ex = find_examples(hard_data, 'HARD2', 3)
hard3_ex = find_examples(hard_data, 'HARD3', 3)

'''
print_examples(hard1_ex)
print_examples(hard2_ex)
print_examples(hard3_ex)
'''

new_hard_data = shuffle(hard_data, random_state = 42)
train_ind = int(9 * round(len(hard_data)/10.))
train_data = new_hard_data[:train_ind]
test_data = new_hard_data[train_ind:]
#print(len(train_data))

hard1_train = [s_inst for (s_inst, sense) in train_data if sense == 'HARD1']
hard2_train = [s_inst for (s_inst, sense) in train_data if sense == 'HARD2']
hard3_train = [s_inst for (s_inst, sense) in train_data if sense == 'HARD3']
'''
print(len(hard1_train))
print(len(hard2_train))
print(len(hard3_train))
'''

si0 = hard1_train[0]
fd = nltk.FreqDist()
fd.update(si0.context)
#print(fd)

si1 = hard1_train[1]
fd.update(si1.context)
#print(fd)

hard1_fd = nltk.FreqDist()
for si in hard1_train:
    hard1_fd.update(si.context)
'''
print(hard1_fd[('time', 'NN')])
print(hard1_fd.N())
'''
# Smoothing
total_V = set(list(hard1_fd.keys()) + list(hard2_fd.keys()) + list(hard3_fd.keys()))
sm_hard_fd = nltk.FreqDist()
for word in total_V:
    sm_hard1_fd[word] = hard1_fd[word] + 1
