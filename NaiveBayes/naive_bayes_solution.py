
from nltk.corpus import senseval
from sklearn.utils import shuffle
import nltk

hard_data = [(i,i.senses[0]) for i in senseval.instances('hard.pos')]

new_hard_data = shuffle(hard_data, random_state = 42)
train_ind = int(9 * round(4333/10.))
train_data = new_hard_data[:train_ind]
test_data = new_hard_data[train_ind:]
tot_num_tokens_target_word = len(train_data)
hard1_train = [s_inst for (s_inst, sense) in  train_data if sense == 'HARD1']
hard2_train = [s_inst for (s_inst, sense) in  train_data if sense == 'HARD2']
hard3_train = [s_inst for (s_inst, sense) in  train_data if sense == 'HARD3']

hard1_fd = nltk.FreqDist()
for si in hard1_train:
    hard1_fd.update(si.context)
hard2_fd = nltk.FreqDist()
for si in hard2_train:
    hard2_fd.update(si.context)
hard3_fd = nltk.FreqDist()
for si in hard3_train:
    hard3_fd.update(si.context)

# t0 is a short example I found after sorting the test data by length.
test_data_s = sorted(test_data, key = lambda x: len(x[0].context))
context_inst = test_data_s[1][0]
t0 = context_inst.context

p_hard1 = float(len(hard1_train))/len(train_data)

#p_hard1
#Out[63]: 0.7954837054144214
# If you got .715, you erroneously divided by len(hard_data)
# If you got .7669, you erroneously counted instances of the word 'hard'.
# The data includes 'harder' and 'hardest', which should be counted too.
# The idea is that 'hard/harder/hardest' are all FORMS of the same word,
# and each of those forms can occur in any of the word's senses.
# Since each sentence contains exactly 1 form of the word, counting sentences
# gives the most accurate result.

p_hard2 = float(len(hard2_train))/len(train_data)
p_hard3 = float(len(hard3_train))/len(train_data)

#p_hard1 + p_hard2 + p_hard3
#Out[67]: 1.0

def compute_joint_prob_simple (sentence, sense_fd, sense_prob):
    """
    Computes naive Bayes unnormalized prob of a sentence,
    using `sense_fd` and `sense_prob` as the
    word frequency distribution and sense probability for that sense.
    """
    # Use sense probability as initial probability
    # Get total number of words using sense from sense_fd.N()
    prob, N = sense_prob, float(sense_fd.N())
    for wd in sentence:
            prob *= sense_fd[wd]/N
    return prob
    

def compute_joint_prob (context_inst, sense_fd, sense_prob):
    """
    Computes naive Bayes unnormalized prob of a sense for the context
    instance `context_inst`, where `sense_fd` and `sense_prob` are the
    word frequency distribution and sense probability for that sense.
    """
    context, position = context_inst.context, context_inst.position
    prob, N = sense_prob, float(sense_fd.N())
    for (i, wd) in enumerate(context):
        if i != position:
            prob *= sense_fd[wd]/N
    return prob
    
##############################################################################
#
#   Q u e s t i o n s
#
##############################################################################


# Question 1
p_hard1 = float(len(hard1_train))/len(train_data)
#p_hard1
#Out[63]: 0.7954837054144214

p_hard2 = float(len(hard2_train))/len(train_data)
#p_hard2
#Out[64]: 0.11778290993071594

p_hard3 = float(len(hard3_train))/len(train_data)
#p_hard3
#Out[65]:  0.08673338465486272

#p_hard1 + p_hard2 + p_hard3
#Out[67]: 1.0


###  Question 2
jp_hard1 = compute_joint_prob (context_inst, hard1_fd, p_hard1)
#Out[68]: 4.304562139450398e-11

## Question 3
jp_hard2 = compute_joint_prob (context_inst, hard2_fd, p_hard2)
# jp_hard2  Beacuse of 0 count for ("watch","NN")
#Out[70]: 0.0  
jp_hard3 = compute_joint_prob (context_inst, hard3_fd, p_hard3)
# jp_hard3
#Out[72]: 9.710982220719328e-14


## Question 4
total_V = set(list(hard1_fd.keys()) + list(hard2_fd.keys()) + list(hard3_fd.keys()))
sm_hard1_fd = nltk.FreqDist()
for word in total_V:
    sm_hard1_fd[word] = hard1_fd[word] + 1

sm_hard2_fd = nltk.FreqDist()
for word in total_V:
    sm_hard2_fd[word] = hard2_fd[word] + 1

sm_hard3_fd = nltk.FreqDist()
for word in total_V:
    sm_hard3_fd[word] = hard3_fd[word] + 1

sm_jp_hard1 = compute_joint_prob (context_inst, sm_hard1_fd, p_hard1)
sm_jp_hard2 = compute_joint_prob (context_inst, sm_hard2_fd, p_hard2)
sm_jp_hard3 = compute_joint_prob (context_inst, sm_hard3_fd, p_hard3)
#print sm_jp_hard1, sm_jp_hard2, sm_jp_hard2
#2.35545947604e-11 5.57420500776e-15 1.961643183651501e-15
# Classification decision 'HARD1' (by several orders of magnitude!)
# Note that you have to compute all three joint probs to find
# the largest, because they do no add up to 1.

## Question 5

p_hard1_given_t1n = sm_jp_hard1/sum([sm_jp_hard1,sm_jp_hard2,sm_jp_hard3])

# sum([sm_jp_hard1,sm_jp_hard2,sm_jp_hard3])
# 2.3562130608634508e-11
#p_hard1_given_t1n
#Out[87]: 0.9996801711900942
# Note that we did not have to compute p('HARD1'| t_{1,n}) to make the decision.





