'''
Erick Pulido
Dr. Gawron
CS581
14 March 2019
Naive Bayes
'''
import nltk
import math
from nltk.corpus import senseval
from sklearn.utils import shuffle

# Data collect
hard_data = [(i, i.senses[0]) for i in senseval.instances('hard.pos')]
ex0 = hard_data[0]
cls = ex0[1]
si = ex0[0]

def find_examples(data, sense, num_examples):
    res = []
    for(inst, s) in data:
        if s == sense:
            res.append(inst)
        if len(res) == num_examples:
            return res
            
def print_examples(ex_list):
    for ex in ex_list:
        for word in ex.context:
            print('{0}_{1}'.format(word[0], word[1]))
        print()
        print()

# TRAINING

hard1_ex = find_examples(hard_data, 'HARD1', 3)
hard2_ex = find_examples(hard_data, 'HARD2', 3)
hard3_ex = find_examples(hard_data, 'HARD3', 3)

new_hard_data = shuffle(hard_data, random_state = 42)
train_ind = int(9 * round(len(hard_data)/10.))
train_data = new_hard_data[:train_ind]
test_data = new_hard_data[train_ind:]

hard1_train = [s_inst for (s_inst, sense) in train_data if sense == 'HARD1']
hard2_train = [s_inst for (s_inst, sense) in train_data if sense == 'HARD2']
hard3_train = [s_inst for (s_inst, sense) in train_data if sense == 'HARD3']

# Begin training set sense 1
si_01 = hard1_train[0]
fd = nltk.FreqDist()
fd.update(si_01.context)

si1 = hard1_train[1]
fd.update(si1.context)
hard1_fd = nltk.FreqDist()

for si in hard1_train:
    hard1_fd.update(si.context)
hard1_n = len(hard1_train)
# End training set sense 1
# Begin training set sense 2
si_02 = hard2_train[0]
fd2 = nltk.FreqDist()
fd2.update(si_02.context)

si2 = hard2_train[1]
fd2.update(si2.context)
hard2_fd = nltk.FreqDist()

for si in hard2_train:
    hard2_fd.update(si.context)
hard2_n = len(hard2_train)
# End training set sense 2
# Begin training set sense 3
si_03 = hard3_train[0]
fd3 = nltk.FreqDist()
fd3.update(si_03.context)

si3 = hard3_train[1]
fd3.update(si3.context)
hard3_fd = nltk.FreqDist()

for si in hard3_train:
    hard3_fd.update(si.context)
hard3_n = len(hard3_train)
# End training set sense 3

total_hard_n = len(train_data)

# Prior probabilities
prior_p_hard1 = float(hard1_n) / total_hard_n
prior_p_hard2 = float(hard2_n) / total_hard_n
prior_p_hard3 = float(hard3_n) / total_hard_n
print("Question 1 answers:")
print("P_hat('HARD1') = ", prior_p_hard1)	# 0.7954837054144214
print("P_hat('HARD2') = ", prior_p_hard2)	# 0.11778290993071594
print("P_hat('HARD3') = ", prior_p_hard3)	# 0.08673338465486272

# Counts
count_hard1 = hard1_fd.N()
count_hard2 = hard2_fd.N()
count_hard3 = hard3_fd.N()
count_total = count_hard1 + count_hard2 + count_hard3

# P (tk|sn)
p_tk_given_hard1 = float(count_hard1) / count_total
p_tk_given_hard2 = float(count_hard2) / count_total
p_tk_given_hard3 = float(count_hard3) / count_total

# Smoothing
total_V = set(list(hard1_fd.keys()) + list(hard2_fd.keys()) + list(hard3_fd.keys()))
V = count_total
V_Hard_N = V * V
N = 0

smoothed_hard1_fd = nltk.FreqDist()
for word in total_V:
	smoothed_hard1_fd[word] = hard1_fd[word] + 1
	
smoothed_hard2_fd = nltk.FreqDist()
for word in total_V:
	smoothed_hard2_fd[word] = hard2_fd[word] + 1

smoothed_hard3_fd = nltk.FreqDist()
for word in total_V:
	smoothed_hard3_fd[word] = hard3_fd[word] + 1

smoothed_count_hard1 = smoothed_hard1_fd.N()
smoothed_count_hard2 = smoothed_hard2_fd.N()
smoothed_count_hard3 = smoothed_hard3_fd.N()
smoothed_total_count = smoothed_count_hard1 + smoothed_count_hard2 + smoothed_count_hard3

t0 = [('it','PRP'),('s','VBZ'),('hard','JJ'),('to','TO'),('watch','VB'),('.','.')]

# Calculation
product_log1 = (hard1_fd[('it','PRP')] / float(count_hard1)) * (hard1_fd[("'s",'VBZ')] / float(count_hard1)) * (hard1_fd[('to','TO')] / float(count_hard1)) * (hard1_fd[('watch','VB')] / float(count_hard1)) * (hard1_fd[('.','.')] / float(count_hard1))

joint_prob_1 = prior_p_hard1 * product_log1 # 4.304562e-11
print("Question 2 answer:")
print("Joint Probability of P_hat('HARD1') and P(t_k | 'HARD1') = %4e" % joint_prob_1)

product_log2 = (hard2_fd[('it','PRP')] / float(count_hard2)) * (hard2_fd[("'s",'VBZ')] / float(count_hard2)) * (hard2_fd[('to','TO')] / float(count_hard2)) * (hard2_fd[('watch','VB')] / float(count_hard2)) * (hard2_fd[('.','.')] / float(count_hard2))	   

joint_prob_2 = prior_p_hard2 * product_log2 # 0.000000e+00
print("Question 3 answers:")
print("Joint Probability of P_hat('HARD2') and P(t_k | 'HARD2') = %4e" % joint_prob_2)

product_log3 = (hard3_fd[('it','PRP')] / float (count_hard3)) * (hard3_fd[("'s",'VBZ')] / float(count_hard3)) * (hard3_fd[('to','TO')] / float(count_hard3)) * (hard3_fd[('watch','VB')] / float(count_hard3)) * (hard3_fd[('.','.')] / float(count_hard3))
			   
joint_prob_3 = prior_p_hard3 * product_log3 # 9.710982e-14
print("Joint Probability of P_hat('HARD3') and P(t_k | 'HARD3') = %4e" % joint_prob_3)

# Smoothing, slides 10 and 16
smoothed_product_log1 = ((smoothed_hard1_fd[('it','PRP')] + 1) / (float(smoothed_count_hard1) + V)) * ((smoothed_hard1_fd[("'s",'VBZ')] + 1) / (float(smoothed_count_hard1) + V)) * ((smoothed_hard1_fd[('to','TO')] + 1) / (float(smoothed_count_hard1) + V)) * ((smoothed_hard1_fd[('watch','VB')] + 1) / (float(smoothed_count_hard1) + V)) * ((smoothed_hard1_fd[('.','.')] + 1) / (float(smoothed_count_hard1) + V))
			   
smoothed_prob_1 = prior_p_hard1 * smoothed_product_log1 # 6.819732e-13
print("Question 4 answers:")
print("Joint Probability of Smoothed P_hat('HARD1') and P(t_k | 'HARD1') = %4e" % smoothed_prob_1)

smoothed_product_log2 = ((smoothed_hard2_fd[('it','PRP')] + 1) / (float(smoothed_count_hard2) + V)) * ((smoothed_hard2_fd[("'s",'VBZ')] + 1) / (float(smoothed_count_hard2) + V)) * ((smoothed_hard2_fd[('to','TO')] + 1) / (float(smoothed_count_hard2) + V)) * ((smoothed_hard2_fd[('watch','VB')] + 1) / (float(smoothed_count_hard2) + V)) * ((smoothed_hard2_fd[('.','.')] + 1) / (float(smoothed_count_hard2) + V))
			   
smoothed_prob_2 = prior_p_hard2 * smoothed_product_log2 # 4.264235e-18
print("Joint Probability of Smoothed P_hat('HARD2') and P(t_k | 'HARD2') = %4e" % smoothed_prob_2)

smoothed_product_log3 = ((smoothed_hard3_fd[('it','PRP')] + 1) / (float(smoothed_count_hard3) + V)) * ((smoothed_hard3_fd[("'s",'VBZ')] + 1) / (float(smoothed_count_hard3) + V)) * ((smoothed_hard3_fd[('to','TO')] + 1) / (float(smoothed_count_hard3) + V)) * ((smoothed_hard3_fd[('watch','VB')] + 1) / (float(smoothed_count_hard3) + V)) * ((smoothed_hard3_fd[('.','.')] + 1) / (float(smoothed_count_hard3) + V))
			   
smoothed_prob_3 = prior_p_hard3 * smoothed_product_log3 # 6.234002e-19
print("Joint Probability of Smoothed P_hat('HARD3') and P(t_k | 'HARD3') = %4e" % smoothed_prob_3)

# Normalize, slide 21
conditional_prob = joint_prob_1 / (joint_prob_1 + joint_prob_2 + joint_prob_3)
# 0.9977491035113332

print("Question 5 answer:")
print("P_hat('HARD1' |t_(1,nd)) for unsmoothed model of t0 = ", conditional_prob)
