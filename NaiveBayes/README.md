# Naive Bayes Assignment

> ### Let’s start with some data that will give us examples of the kind of classes we’re looking for. The Python output in this assignment prompt will assume you are using IPython as your interface to Python, but nothing in the assignment itself depends on that. Any Python interface should work, assuming you have nltk installed and have downloaded the nltk.corpus senseval. For directions on how to download an NLTK corpus, see Pollard assignment.

[Full assignment source](https://gawron.sdsu.edu/compling/course_core/comp_ling_assignments/naive_bayes_assignment.html)
